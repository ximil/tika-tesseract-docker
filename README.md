# Image based on https://github.com/mattfullerton/tika-tesseract-docker with spanish support on ocr 
# Apache Tika Server w/ Tesseract in Docker with spanish support

Sets up a container based on
[java:7](https://hub.docker.com/_/java/)


## Includes

  * [Apache Tika Server](http://wiki.apache.org/tika/TikaJAXRS) - latest development version (1.13-SNAPSHOT currently)
  * [Tesseract](https://code.google.com/p/tesseract-ocr/), with English and Spanish languages

You can find latest stable version of Tika-server (including OCR via Tesseract), on
[`logicalspark/docker-tikaserver`](https://github.com/LogicalSpark/docker-tikaserver)

## Usage

To use the image from the Docker registry, just do:

    sudo docker run -d -p 9998:9998 ximil/tika-tesseract-docker